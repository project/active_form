<?php

namespace Drupal\active_form\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a im_universal_variable Collector backend annotation object.
 *
 * @see plugin_api
 *
 * @Annotation
 */
class ActiveForm extends Plugin {

  /**
   * The backend plugin ID.
   *
   * @var string
   */
  public $form_id;

  /**
   * {@inheritdoc}
   */
  public function getId(): string {
    return $this->definition['form_id'];
  }

}
