<?php

namespace Drupal\active_form\Plugin\ActiveForm;

abstract class BaseVueForm extends BaseForm {

  /**
   * @return array
   */
  protected function build(): array {
    return ['#tag' => $this->getComponentName()] + parent::build();
  }

  /**
   * @return array
   */
  protected function getHtmlAttributes(): array {
    return array_diff_key(parent::getHtmlAttributes(), ['is' => 0]);
  }
}
