<?php

namespace Drupal\active_form;

class BaseException extends \Exception {
  private $data = [];

  /**
   * @param array $data
   *
   * @return BaseException
   */
  public function setData(array $data): BaseException {
    $this->data = $data;

    return $this;
  }

  /**
   * @param array $data
   *
   * @return BaseException
   */
  public function addData(array $data): BaseException {
    $this->data = array_merge($this->data, $data);

    return $this;
  }

  /**
   * @param $key
   * @param $value
   *
   * @return BaseException
   */
  public function addValue($key, $value): BaseException {
    $this->data[$key] = $value;

    return $this;
  }

  /**
   * @return array
   */
  public function getData(): array {
    return $this->data;
  }
}
