<?php


namespace Drupal\active_form\Element;


use Drupal\Core\Render\Element\RenderElement;

/**
 * Class ActiveForm
 *
 * @package Drupal\active_form\Element
 *
 * @RenderElement("active_form")
 */
class ActiveForm extends RenderElement {

  /**
   * Returns the element properties for this element.
   *
   * @return array
   *   An array of element properties. See
   *   \Drupal\Core\Render\ElementInfoManagerInterface::getInfo() for
   *   documentation of the standard properties of all elements, and the
   *   return value format.
   */
  public function getInfo() {
    // TODO: Implement getInfo() method.
    return [
      '#pre_render' => [
        [static::class, 'preRender'],
      ],
    ];
  }

  /**
   * @param array $form
   *
   * @return array
   */
  public static function preRender(array $form): array {
    return $form + [
        'form' =>
          \Drupal::service('plugin.manager.active_form')
            ->createInstance($form['#form_id'], $form['#conf'] ?? [])
            ->toRenderable(),
      ];
  }
}
