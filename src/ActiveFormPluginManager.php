<?php

namespace Drupal\active_form;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 */
class ActiveFormPluginManager extends DefaultPluginManager {

  /**
   * @param \Traversable $namespaces
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/ActiveForm', $namespaces, $module_handler, 'Drupal\active_form\Plugin\ActiveForm\ActiveFormInterface', 'Drupal\active_form\Annotation\ActiveForm');
    $this->setCacheBackend($cache_backend, 'active_form:plugins');
  }

  public function controller(Request $request) {
    $form_id = $request->get('form_id');
    $plugin_definition = $this->getDiscovery()->getDefinition($form_id);
    $plugin_class = $plugin_definition["class"];
    $conf = \GuzzleHttp\json_decode($request->get('storage_json', '[]'), TRUE);

    return (new $plugin_class($conf, $form_id, $plugin_definition))->handle($request);
  }

}
