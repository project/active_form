class LighterForm extends HTMLFormElement {
  constructor() {
    super();
    this.disabled = false;
    this.uuid = LighterForm.generateUUID();
    this.addEventListener('submit', this.onSubmit);
    this.addEventListener('click', e => {
      if (e.target.matches('button[name=op], input[type=submit][name=op]')) {
        this._op = e.target.value;
      }
    });
    let props = Object.assign({}, this.props(), LighterForm.props());
    for (let prop_name in props) {
      if (!props.hasOwnProperty(prop_name)) {
        continue
      }
      let prop = props[prop_name];
      if (!prop.type) {
        prop = {type: prop};
      }
      if (prop.type === String) {
        Object.defineProperty(this, prop_name, {
          set: (value) => this.setAttribute(prop_name, value),
          get: () => this.getAttribute(prop_name),
        });
      }
      if (prop.type === Boolean) {
        Object.defineProperty(this, prop_name, {
          set: (value) => value ? this.setAttribute(prop_name, prop_name) : this.removeAttribute(prop_name),
          get: () => this.hasAttribute(prop_name),
        });
      }
      if (prop.type === JSON) {
        Object.defineProperty(this, prop_name, {
          set: (value) => this.setAttribute(prop_name, JSON.stringify(value)),
          get: () => JSON.parse(this.getAttribute(prop_name)),
        });
      }
    }
  }

  static props() {
    return {
      csrf_token: String,
      storage_json: String,
      storage: JSON,
      storage_token: String,
    }
  }

  props() {
    return {}
  }

  connectedCallback() {
    this.className = (this.className + ' ' + this.b()).trim();
    setTimeout(() => {
      const json = this.querySelector('script[type="application/json"]');
      if (json) {
        Object.assign(this, JSON.parse(json.textContent));
      }
      this.render();
    }, 0);
  }

  render() {
    const {render, html} = lighterhtml;
    render(this, () => html`<h5>${this.form_id}</h5>
<p>storage_json: ${this.storage_json}</p>`)
  }

  get form_id() {
    return 'lighter-form'
  }

  b() {
    return 'f-' + this.form_id
  }

  e(el) {
    return this.b() + '__' + el
  }

  getId(suffix, ...mod) {
    return this.uuid + (suffix ? ('__' + suffix) : '') + (mod.length ?
      ('--' + mod.join('--')) : '');
  }

  static generateUUID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
      const r = Math.random() * 16 | 0,
        v = c === 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    })
  }

  static goto(destination) {
    destination = (destination === '_self') ? window.location : destination;
    if (window.location.pathname + window.location.search === destination.replace(/#.*$/, '')) {
      window.location = destination;
      window.location.reload();
    }
    else {
      window.location = destination;
    }
  }

  onSubmit(e) {
    e.preventDefault();
    if (this.disabled) {
      return;
    }
    this.disabled = true;
    const formData = new FormData(this);
    formData.set('form_id', this.form_id);
    formData.set('csrf_token', this.csrf_token);
    formData.set('storage_json', this.storage_json);
    formData.set('storage_token', this.storage_token);
    formData.set('op', this._op);
    fetch(this.action, {
      method: "POST",
      body: formData
    })
      .then(response => response.json())
      .then(response => {
        this.disabled = false;
        this.handle(response);
        this.render();
      })
      .catch(error => {
        this.disabled = false;
        console.log(error);
        this.render();
      });
    this.render();
  }

  handle(resp) {
    if (resp.goto) {
      LighterForm.goto(resp.goto);
      return
    }
    if (resp.attributes) {
      Object.assign(this, resp.attributes);
    }
    if (resp.set) {
      Object.assign(this, resp.set);
    }
  }
}
