Vue.use({
  install: function (Vue, options) {
    Vue.ActiveForm = function (form, opt) {
      form.mixins = form.mixins || [];
      form.mixins.push({
        props: {
          storage_token: String,
          storage_json: String,
          csrf_token: String,
        },
        methods: {
          b() {
            return 'f-' + form.form_id
          },
          e(el) {
            return this.b() + '__' + el
          },
          getId(suffix, ...mod) {
            return this.uuid + (suffix ? ('__' + suffix) : '') + (mod.length ?
              ('--' + mod.join('--')) : '');
          },
          goto(destination) {
            destination = (destination === '_self') ? window.location : destination;
            if (window.location.pathname + window.location.search === destination.replace(/#.*$/, '')) {
              window.location = destination;
              window.location.reload();
            }
            else {
              window.location = destination;
            }
          },
          handle(resp) {
            if (resp.goto) {
              this.goto(resp.goto);
              return
            }
            if (resp.attributes) {
              Object.assign(this, resp.attributes);
            }
            if (resp.set) {
              Object.assign(this, resp.set);
            }
          }
        },
        computed: {
          storage() {
            return JSON.parse(this.storage_json);
          }
        },
        data() {
          return Object.assign({
            uuid: 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
              const r = Math.random() * 16 | 0,
                v = c === 'x' ? r : (r & 0x3 | 0x8);
              return v.toString(16);
            }),
            disabled: false,
            op: '',
            message: {},
            error: {},
          })
        },
        mounted() {
          this.$el.addEventListener('submit', e => {
            e.preventDefault();
            if (this.disabled) {
              return;
            }
            this.disabled = true;
            const formData = new FormData(this.$el);
            formData.set('form_id', form.form_id);
            formData.set('csrf_token', this.csrf_token);
            formData.set('storage_json', this.storage_json);
            formData.set('storage_token', this.storage_token);
            formData.set('op', this.op);
            fetch(this.$el.action, {
              method: "POST",
              body: formData
            })
              .then(response => response.json())
              .then(response => {
                this.disabled = false;
                this.handle(response)
              })
              .catch(error => {
                this.disabled = false;
                console.log(error)
              });
          });
          this.$el.addEventListener('click', e => {
            if (e.target.matches('button[name=op], input[type=submit][name=op]')) {
              this.op = e.target.value;
            }
          });
        }
      });
      Vue.customElement(form.is, form, opt);
    };
  }
});
